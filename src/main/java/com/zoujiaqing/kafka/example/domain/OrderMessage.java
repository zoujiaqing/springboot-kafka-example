package com.zoujiaqing.kafka.example.domain;

import lombok.Builder;
import lombok.Data;

@Data

public class OrderMessage {
    private Order order;
    private OrderStatus status;
}
