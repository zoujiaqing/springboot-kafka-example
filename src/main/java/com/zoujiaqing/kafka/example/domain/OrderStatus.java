package com.zoujiaqing.kafka.example.domain;

public enum OrderStatus {
    CREATED, PAID, CANCELLED, COMPLETED
}
