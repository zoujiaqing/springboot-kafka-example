package com.zoujiaqing.kafka.example.domain;

import lombok.Builder;
import lombok.Data;

@Data
public class Order {
    private String id;
    private String userId;
    private double price;
    private OrderStatus status;
    private String created;
}
