package com.zoujiaqing.kafka.example.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zoujiaqing.kafka.example.domain.Order;
import com.zoujiaqing.kafka.example.domain.OrderStatus;
import com.zoujiaqing.kafka.example.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping("/create")
    public ResponseEntity<String> create() throws JsonProcessingException {
        String id = "10001";
        Order order = new Order();
        order.setId(id);
        order.setStatus(OrderStatus.CREATED);
        orderService.createOrder(order);
        return ResponseEntity.ok("Created");
    }
}
